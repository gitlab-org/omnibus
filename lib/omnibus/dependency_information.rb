require "json" unless defined?(JSON)

module Omnibus
  class DependencyInformation

    include Logging
    JSON_FILE = "dependency_licenses.json".freeze

    class << self
      def process!(project, json_file_path = nil)
        new(project, json_file_path).process!
      end
    end

    attr_reader :project

    def initialize(project, json_file_path = nil)
      @project = project
      @final_output = []
      @json_file = json_file_path || default_json_file
    end

    def default_json_file
      File.expand_path(JSON_FILE, project.install_dir)
    end

    def licenses_dir
      File.expand_path("LICENSES", project.install_dir)
    end

    def license_package_location(component_name, filename)
      # Duplicated from licensing.rb
      File.join(licenses_dir, "#{component_name}-#{File.split(filename).last}")
    end

    def process!
      collect_licenses
      write_dependency_licenses_file
      write_project_license_file
    end

    def collect_licenses
      # Handling all libraries
      project.library.each do |component|
        result = {
          name: component.name,
          version: component.version,
          license: component.license,
          license_texts: [],
          dependencies: [],
        }

        license_texts = []
        component.license_files.each do |license_file|

          # Instead of reading the license file from the source directory, we
          # are reusing the license file created by omnibus's licensing feature
          # at /opt/gitlab/LICENSES/#{component_name}-#{license_file} because
          # of one edge case - when software whose license file was added by a
          # patch (like config_guess) was restored from cache, the source
          # directory won't have it. But because it is present in the git
          # cache, it will be present in the output license directory.
          license_file_path = license_package_location(component.name, license_file)
          license_texts << File.read(license_file_path) if File.exist?(license_file_path)
        end
        result[:license_texts] = license_texts.flatten

        component.dependencies.each do |name|
          dependency = project.library.find { |software| software.name == name }

          item = {
            name: name,
            version: dependency.version,
            license: dependency.license,
            license_texts: [],
          }

          # Technically, this is redundant because the license information
          # about internal dependencies is already present in the top-level of
          # the final_output map. However, to maintain consistency between
          # internal and external dependencies, we're intentionally duplicating
          # this information. We do realize this increases the size of the
          # file.
          license_texts = []
          dependency.license_files.each do |license_file|
            license_file_path = license_package_location(name, license_file)
            license_texts << File.read(license_file_path) if File.exist?(license_file_path)
          end
          item[:license_texts] = license_texts.flatten

          result[:dependencies] << item
        end

        @final_output << result
      end

      # Handling external dependencies of those libraries, if provided explicitly
      json_licenses_dir = File.expand_path("licenses", project.install_dir)
      Dir.glob(File.expand_path("*.json", json_licenses_dir)).each do |json_file|
        # Getting the software name from file name
        component = File.basename(json_file, File.extname(json_file))

        # Find index of the software in final output
        index = @final_output.each_index.find { |software_index| @final_output[software_index][:name] == component }

        # Parse JSON file from license_finder
        dependencies_info = JSON.parse(File.read(json_file))["dependencies"]
        dependencies_info.map do |info|
          license_texts = []
          %w{texts notice}.each do |type|
            license_texts << info[type] if info[type] && !info[type].empty?
          end

          item = {
            name: info["name"],
            version: info["version"],
            license: info["licenses"].join(", "),
            license_texts: license_texts.flatten,
          }

          @final_output[index][:dependencies] << item
        end
      end
    end

    def write_dependency_licenses_file
      File.open(@json_file, "w") do |f|
        f.write(JSON.pretty_generate(@final_output))
      end
    end

    def get_project_license_text
      text = []
      text << "Name: #{project.name}"
      text << "Version: #{project.build_version}"
      text << "License: '#{project.license}'"
      unless project.license_file.nil?
        text << "License text:"
        text << "  #{File.read(File.join(Config.project_root, project.license_file)).gsub("\n", "\n  ")}"
        text << ""
      end

      text.join("\n")
    end

    def get_component_license_text(component)
      text = []
      text << "Name: #{component[:name]}"
      text << "Version: #{component[:version]}"
      text << "License(s): #{component[:license]}"
      unless component[:license_texts].empty?
        text << "License text:"
        text << "  #{component[:license_texts].join("\n").gsub("\n", "\n  ")}"
        text << ""
      end

      text.join("\n")
    end

    def write_project_license_file
      completed_components = []
      File.open(project.license_file_path, "w") do |f|
        f.puts "============================================================="
        f.puts get_project_license_text
        f.puts "============================================================="

        f.puts ""
        f.puts "License details of the components included in the package are listed below:"
        f.puts "Note: The following list is generated programmatically and not verified manually."
        f.puts "      Hence, it is possible that license texts of some of the components included"
        f.puts "      in the package are missing from this file. If you come across such a component"
        f.puts "      please open an issue in `https://gitlab.com/gitlab-org/omnibus-gitlab/issues"
        f.puts ""

        # Tackle level-1 dependencies, collected from component software
        # definitions
        @final_output.each do |component|
          f.puts "=========="
          f.puts get_component_license_text(component)
          completed_components << component[:name]

          # Tackle level-2 dependencies, collected from license_finder
          component[:dependencies].each do |dependency|
            # If the component was already handled at level-1, we ignore it
            next if completed_components.include?(dependency[:name])

            f.puts "=========="
            f.puts get_component_license_text(dependency)
            completed_components << dependency[:name]
          end
        end
        f.puts "=========="
        f.puts ""
      end
    end
  end
end
